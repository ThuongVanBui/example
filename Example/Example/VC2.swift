//
//  VC2.swift
//  Example
//
//  Created by Bùi Văn Thương on 1/2/19.
//  Copyright © 2019 Bùi Văn Thương. All rights reserved.
//

import UIKit

class VC2CLC: UICollectionViewCell {
    
    @IBOutlet weak var imgV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

class VC2: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var clv: UICollectionView!
    
    let data = ["1","2","3","4","5","6","7","8"]
    
    var index: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clv.delegate = self
        self.clv.dataSource = self
        self.clv.alwaysBounceVertical = true
        
        
        self.clv.reloadData()
        // Do any additional setup after loading the view.
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        print("Sessions View Controller Will Disappear")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Sessions View Controller Will Appear")

        self.clv.scrollToItem(at: self.index ?? IndexPath(row: 0, section: 0), at: .top, animated: false)
        self.clv.layoutIfNeeded()

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VC2CLC", for: indexPath) as! VC2CLC
        cell.imgV.image = UIImage(named: data[indexPath.row])
        return cell
    }
    
    //    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    //        <#code#>
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/3, height: UIScreen.main.bounds.width/3)
    }
    

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll: \(scrollView.contentOffset.y)")
    }
}
