//
//  MainViewController.swift
//  Example
//
//  Created by Bùi Văn Thương on 1/2/19.
//  Copyright © 2019 Bùi Văn Thương. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    
    var isGrid:Bool = false
    @IBOutlet weak var btn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.updateView()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func changeV(_ btn: UIBarButtonItem){
        self.isGrid = !self.isGrid
        self.btn.title = (self.isGrid == true) ? "Lists":"Grid"
        self.updateView()
    }
    
    
    private lazy var summaryViewController: VC1 = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "VC1") as! VC1
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var sessionsViewController: VC2 = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "VC2") as! VC2
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()

    fileprivate func add(asChildViewController viewController: UIViewController) {
        // Add Child View as Subview
        view.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = view.bounds
        //viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
    private func updateView() {
        if self.isGrid == false {
            remove(asChildViewController: sessionsViewController)
            add(asChildViewController: summaryViewController)
        } else {
            remove(asChildViewController: summaryViewController)
            add(asChildViewController: sessionsViewController)
        }
    }
}
