//
//  VC1.swift
//  Example
//
//  Created by Bùi Văn Thương on 1/2/19.
//  Copyright © 2019 Bùi Văn Thương. All rights reserved.
//

import UIKit

class VC1CLC: UICollectionViewCell {
    @IBOutlet weak var imgV:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

class VC1: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var clv: UICollectionView!

    let data = ["1","2","3","4","5","6","7","8"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clv.delegate = self
        self.clv.dataSource = self
        self.clv.alwaysBounceVertical = true
        
        
        self.clv.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("Summary View Controller Will Appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        print("Summary View Controller Will Disappear")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VC1CLC", for: indexPath) as! VC1CLC
        cell.imgV.image = UIImage(named: data[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "VC2") as? VC2
        vc?.index = indexPath
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height:2*(UIScreen.main.bounds.width/3))
        
    }
    

}
